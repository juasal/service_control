<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ServiceFormRequest;
use DB;

class ServiceController extends Controller
{
    public function __construct(){
    }

    public function index(Request $request){

        if($request){
            $query=trim($request->get('searchText'));
            $servicios=DB::table('servicios')->where('name','LIKE','%'.$query.'%')
                                             ->where('status','=','1')
                                             ->orderBy('id','desc')
                                             ->paginate(7);
            return view('servicios.index',["servicios"=>$servicios,"searchText"=>$query]);
        }
   }

   public function create(){
        return view('servicios.create');
   }

   public function store(ServiceFormRequest $request){

               
        $servicio = new Service;
        $servicio->user_id = Auth::id();
        $servicio->name = $request->get('name');
        $servicio->status = $request->get('status');
        $servicio->save();
        return Redirect::to('servicios');
   }

   public function show($id){
        return view("servicios.show",["servicios"=>Service::findOrFail($id)]);
   }

   public function edit($id){
    return view("servicios.edit",["servicio"=>Service::findOrFail($id)]);
   }

   public function update(ServiceFormRequest $request, $id){
       $servicio=Service::findOrFail($id);
       $servicio->name = $request->get('name');
       $servicio->status = $request->get('status');
       $servicio->update();
       return Redirect::to('servicios');   
   }

   public function destroy($id){
        $servicio = Service::findOrFail($id);
        $servicio->status=0;
        $servicio->update();
        return Redirect::to('servicios');
   }
}
