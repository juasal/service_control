<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{

    protected $table = 'rol';
    protected $primaryKey = 'Id';
        
    public function Rol(){

        return $this->hasOne('App\User');
    }
}
