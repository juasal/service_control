@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>Listado de servicios {{ Auth::id() }}</h3><a href="servicios/create"><button class="btn btn-success">Nuevo</button></a>
                    @include('servicios.search')
                </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    
                    </div>
                    <div class="col-lg-8 col-md-8">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-condensed table-hove">
                              <thead>
                                  <th>Id</th>
                                  <th>Nombre</th>
                                  <th>Status</th>
                                  <th>Opciones</th>                                 
                              </thead> 
                              @foreach ($servicios as $serv)
                                  <tr>
                                      <td>{{ $serv->id}}</td>
                                      <td>{{ $serv->name}}</td>
                                      <td>{{ $serv->status}}</td>
                                      <td>
                                      <a href="{{URL::action('ServiceController@edit',$serv->id)}}"><button class="btn btn-info">Editar</button></a>
                                      <a href="" data-target="#modal-delete-{{$serv->id}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
                                      </td>
                                  </tr>
                                  @include('servicios.modal');
                              @endforeach 
                            </table>
                        </div>
                        {{$servicios->render()}}
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
