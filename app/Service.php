<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
   protected $table = 'servicios';
   protected $primaryKey = 'id';

   protected $fillable =[
       'user_id',
       'name',
       'status'
   ];
    public function Service(){

        return $this->hasOne('App\User');
    }
}
