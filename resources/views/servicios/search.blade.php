{!! Form::open(array('url'=>'servicios','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}
    <div class="row">
        <div class="input-group">
        <input type="text" class="form-control" placeholder="Buscar..." name="searchText" value="{{$searchText}}">
        <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">Buscar</button>
        </span>

        </div>
    </div>
{{Form::close()}}